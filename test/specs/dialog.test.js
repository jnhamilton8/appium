
import dialog from '../../pageObjects/dialog.page';
import { expect } from 'chai';

describe('Dialog', () => {

    afterEach(() => {
        driver.reset(); //reset the device after each test
    });


    it('Verify that the app can be scrolled using touchAction', () => {
        dialog.viewBtn.click();
        driver.touchAction([
            { action: 'press', x: 300, y: 1600 },
            { action: 'moveTo', x: 300, y: 300 },
            'release'
        ])
    });

    it('Verify that the text entry dialog username and password fields are editable', () => {
        dialog.appBtn.click();
        dialog.alertDialogBtn.click();
        dialog.textEntryDialogBtn.click();

        dialog.userNameField.waitForEnabled({ timeout: 2000 });
        dialog.userNameField.addValue("Username");
        dialog.userNameField.clearValue(); 
        dialog.userNameField.addValue("Actual User");
        
        dialog.passwordField.waitForEnabled({ timeout: 2000 });
        dialog.passwordField.clearValue(); 
        dialog.passwordField.addValue("Password");

        const text = dialog.userNameField.getText();
        expect(text).to.equal("Actual User");

        dialog.dialogOkBtn.click();
    });

    it('Verify that the app adjusts when orientation is switched', () => {
        const orientation = driver.getOrientation();
        
        if (orientation == 'PORTRAIT') {
            driver.setOrientation('LANDSCAPE');
        } else {
            driver.setOrientation('PORTRAIT');
        }

        driver.saveScreenshot('./screenshots/landscape.png');
        dialog.appBtn.click();

        driver.setOrientation('PORTRAIT'); //let's make sure it is set back to portrait
        driver.back();
        driver.saveScreenshot('./screenshots/portrait.png');
    });

    it.skip('Verify timeouts', () => {
        driver.setImplicitTimeout(10000); //only waits until is finds the element
        driver.setTimeouts(10000); //waits for the time specified
        driver.pause(10000);  //pause execution for x period of time

        //dialog.viewBtn.click(); //will be present
        dialog.tabsBtn.click();; //will not be present
    });

    it('Verify the repeat alarm options has attribute checked when selected', () => {        
        dialog.appBtn.click();
        dialog.alertDialogBtn.click();
        dialog.repeatAlarmBtn.click();

        const text = dialog.weekdayCheckbox(0).getText();
        expect(text).to.equal('Every Monday');

        let isChecked = dialog.weekdayCheckbox(0).getAttribute('checked');
        expect(isChecked).to.equal('false');

        dialog.weekdayCheckbox(0).click();

        isChecked = dialog.weekdayCheckbox(0).getAttribute('checked');
        expect(isChecked).to.equal('true');
    });

    it('Verify isSelected, isEnabled & isDisplayed', () => {
        dialog.viewBtn.click();
        driver.touchAction([ //scroll 3 times to get to tabs button
            { action: 'press', x: 300, y: 1400 },
            { action: 'moveTo', x: 300, y: 300 },
            'release',
            { action: 'press', x: 300, y: 1400 },
            { action: 'moveTo', x: 300, y: 300 },
            'release',
            { action: 'press', x: 300, y: 1400 },
            { action: 'moveTo', x: 300, y: 300 },
            'release'
        ]);

        dialog.tabsBtn.click();
        dialog.contentByIdBtn.click();

        for(let i=0; i<3; i++) {
            const isEnabled = dialog.tabs[i].isEnabled();
            const isSelected = dialog.tabs[i].isSelected();
            const isDisplayed = dialog.tabs[i].isDisplayed();

            console.log(`Tab ${i+1}`);
            console.log('isEnabled:', isEnabled);
            console.log('isSelected:', isSelected);
            console.log('isDisplayed:', isDisplayed);

            if(isEnabled && isSelected) {
                console.log("Tab details 1:", dialog.tab1Details.isDisplayed());
                console.log("Tab details 2:", dialog.tab2Details.isDisplayed());
                console.log("Tab details 3:", dialog.tab3Details.isDisplayed());
            }
        }
    });
});
