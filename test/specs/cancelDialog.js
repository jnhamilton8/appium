import dialog from '../../pageObjects/dialog.page'; 
import { shortCancelDialog, longCancelDialog, ultraLongCancelDialog } from '../../data/data';
import { expect } from 'chai';
 
describe('Cancel Dialogs', () => {
    before(() => {
        dialog.appBtn.click();
        dialog.alertDialogBtn.click();
    });
    
    it('Verify that the cancel dialog short message is correct and displays', () => {
        dialog.cancelDialogMsgBtn.click();
        expect(dialog.getDialogModalTitle()).equal(shortCancelDialog.title);
        dialog.dialogOkBtn.click();
    });
    
    it('Verify that the cancel dialog long message is correct and displays', () => {
        dialog.cancelLongDialogMsgBtn.click();
        expect(dialog.getDialogModalTitle()).equal(longCancelDialog.title);
        expect(dialog.getDialogModalMsg()).equal(longCancelDialog.msg);
        dialog.dialogOkBtn.click();
    });
    
    it('Verify that the cancel dialog ultra long message is correct', () => {
        dialog.cancelUltraLongDialogMsgBtn.click();
        expect(dialog.getDialogModalTitle()).equal(ultraLongCancelDialog.title);
        expect(dialog.getDialogModalMsg()).equal(ultraLongCancelDialog.msg);
        dialog.dialogOkBtn.click();
    });
});